# this is the solution code for:
# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)

class RangeList
  attr_reader :intervals

  # Chinese: 取反思维,判断不重合更简单直接
  def self.not_overlapped?(r1, r2)
    r1.first > r2.last || r1.last < r2.first
  end

  def initialize(*range)
    raise ArgumentError, 'range must be an Array List' unless range.all? { |r| r.is_a? Array }

    @intervals = range
  end

  # see README.md for algorithm explanation.
  def add(range)
    overlapped_rl = find_overlapped_pairs(range)

    left_min = overlapped_rl.map(&:first).min
    right_max = overlapped_rl.map(&:last).max
    merged_overlapped_pair = [left_min, right_max]
    overlapped_rl.each { |r| intervals.delete(r) }

    index = intervals.map(&:first).bsearch_index { |r| r >= left_min } # using binary search for sorting optimization.
    index ? intervals.insert(index, merged_overlapped_pair) : intervals << merged_overlapped_pair
    # using the Array build in sort method sort_by! is also an simple option with slightly performance trade-off.
    # intervals << [left_min, right_max]
    # intervals.sort_by!(&:first)

    # I want this method's return value to be more intuitive.
    intervals
  end

  def remove(range)
    overlapped_rl = find_overlapped_pairs(range)
    overlapped_rl.each { |r| intervals.delete(r) }

    range_left = range.first
    range_right = range.last
    left_min = overlapped_rl.map(&:first).min
    intervals << [left_min, range_left] if left_min < range_left
    right_max = overlapped_rl.map(&:last).max
    intervals << [range_right, right_max] if right_max > range_right

    intervals.sort_by!(&:first)
    intervals
  end

  # to put out a more readable literal, such like: [2, 5) [6, 8) [9, 12)
  def print
    intervals.inject('') do |sum, r|
      "#{sum}[#{r.first}, #{r.last}) " # using string interpolation is a ruby way.
    end.rstrip # String#chop is also available
  end

  # Returns true if every real number in the range is currently being tracked, and false otherwise.
  # OPTIMIZE: binary search could be a better idea.but linear or iteration is clean and simple.
  def query(range)
    intervals.any? do |r|
      r.first <= range.first && r.last >= range.last
    end
  end

  private

  # find all ranges that overlapped, touched, contained with range, and including range self.
  def find_overlapped_pairs(range)
    # duck typing check. some ohter data types like Range instance cloud also be used.
    raise ArgumentError, 'range must be a interval' unless range.respond_to?(:first) && range.respond_to?(:last)

    overlapped_rl = [range]
    intervals.each do |r|
      next if self.class.not_overlapped?(r, range)

      overlapped_rl << r
    end
    overlapped_rl
  end
end
