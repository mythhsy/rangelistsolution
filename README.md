# About
The solution for the interview question `RangeList` from 极狐Gitlab by @mythhsy.

# Problem
```ruby
# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.

class RangeList
  def add(range)
    # TODO: implement add
  end
  
  def remove(range)
    # TODO: implement remove
  end
  
  def print
    # TODO: implement print
  end
end
```

# Idea
`
Maintain Sorted Disjoint Intervals.
`

## 算法分析

我们尝试维护一个数据结构,存储有序的不相交区间.可以使用数组.

`add(range)`: 当我们要添加一个区间`range`时，我们使用线性或二分查找，找到和`range`相交的区间,包括有部分重合的,包含关系的,相接的这些.随后，我们将这些区间外加`range`的区间组替换成一个新的区间,满足条件: 左端点为区间组中的最小值,右端点为区间组中的最大值. 

`remove(range)`: 当我们要删除一个区间`range`时，我们像`add`一样找到相交区间组,随后根据重合的不同的情况,将其替换成替换成0/1/2个新的区间.

扩展接口: 
`query(range)`

定义: 如果range的区间范围在`rangelist`中则返回`true`,否则返回`false`.

实现：当我们要查找一个区间时,我们只需要进行查找,判断是否有一个区间包含了`range`区间.

## Space and time complexity Analysis

time complexity：设`n`为`ranges`中的元素个数，那么`add`和`remove`的时间复杂度为`O(n)`.

space complexity：如果有`m`次`add`操作,`n`次`remove`操作,空间复杂度为`O(m+n)`.

# Testing

To run the specs, you first need to install rspec using this command:

`gem install rspec`

After installing rspec, enter this command:

`rspec spec.rb`

There are `41` examples for now, You could add additional test cases or specs to this file.

# 其他解法探索
应该还有一些其他解法可以探索,持续学习.目前有初步想法的两种方式:

1. 利用高级数据结构`线段树 Segment Tree`.
2. 数组一维化查找.

