require 'rspec'
require_relative 'solution'

RSpec.describe 'RangeList test cases' do
  describe 'RangeList initialization' do
    it 'initialization an empty RangeList' do
      expect(RangeList.new.intervals).to eq []
    end

    it 'initialization a RangeList with just one pair' do
      expect(RangeList.new([1, 3]).intervals).to eq [[1, 3]]
    end

    it 'initialization a RangeList with more than one pair' do
      expect(RangeList.new([1, 3], [2, 4], [5, 8]).intervals).to eq [[1, 3], [2, 4], [5, 8]]
    end

    it 'argument error' do
      expect { RangeList.new(1, 2) }.
        to(raise_error(ArgumentError, 'range must be an Array List'))
    end
  end

  describe 'add method behavior' do
    let(:example_rl) { RangeList.new [3, 5], [7, 8], [9, 13] }

    it 'not overlapped' do
      expect(example_rl.add([14, 15])).to eq [[3, 5], [7, 8], [9, 13], [14, 15]]
    end

    it 'order is maintained' do
      expect(example_rl.add([1, 2])).to eq [[1, 2], [3, 5], [7, 8], [9, 13]]
    end

    it 'touched on left' do
      expect(example_rl.add([12, 13])).to eq [[3, 5], [7, 8], [9, 13]]
    end

    it 'touched on right' do
      expect(example_rl.add([6, 7])).to eq [[3, 5], [6, 8], [9, 13]]
    end

    it 'contained' do
      expect(example_rl.add([10, 11])).to eq [[3, 5], [7, 8], [9, 13]]
    end

    it 'contain ohter range' do
      expect(example_rl.add([2, 6])).to eq [[2, 6], [7, 8], [9, 13]]
    end

    it 'overlapped' do
      expect(example_rl.add([12, 15])).to eq [[3, 5], [7, 8], [9, 15]]
    end

    it 'just overlapped' do
      expect(example_rl.add([9, 13])).to eq [[3, 5], [7, 8], [9, 13]]
    end

    it 'long term and contained' do
      expect(example_rl.add([6, 15])).to eq [[3, 5], [6, 15]]
    end

    it 'long term and overlapped' do
      expect(example_rl.add([4, 10])).to eq [[3, 13]]
    end

    it 'long term and touched' do
      expect(example_rl.add([7, 13])).to eq [[3, 5], [7, 13]]
    end
  end

  describe 'remove method behavior' do
    let(:example_rl) { RangeList.new [3, 5], [7, 8], [9, 13] }

    it '不重合' do
      expect(example_rl.remove([14, 15])).to eq([[3, 5], [7, 8], [9, 13]])
    end

    it '顺序不变' do
      expect(example_rl.remove([1, 2])).to eq([[3, 5], [7, 8], [9, 13]])
    end

    it '左接触' do
      expect(example_rl.remove([13, 15])).to eq([[3, 5], [7, 8], [9, 13]])
    end

    it '右接触' do
      expect(example_rl.remove([1, 3])).to eq([[3, 5], [7, 8], [9, 13]])
    end

    it '被包含' do
      expect(example_rl.remove([10, 12])).to eq([[3, 5], [7, 8], [9, 10], [12, 13]])
    end

    it '重合' do
      expect(example_rl.remove([12, 15])).to eq([[3, 5], [7, 8], [9, 12]])
    end

    it '刚好重合' do
      expect(example_rl.remove([9, 13])).to eq([[3, 5], [7, 8]])
    end

    it '包含' do
      expect(example_rl.remove([2, 6])).to eq([[7, 8], [9, 13]])
    end

    it '长跨越且包含' do
      expect(example_rl.remove([2, 15])).to eq([])
    end

    it '长跨越且重合' do
      expect(example_rl.remove([6, 12])).to eq([[3, 5], [12, 13]])
    end

    it '长跨越且接触' do
      expect(example_rl.remove([7, 13])).to eq([[3, 5]])
    end
  end

  describe 'query method behavior' do
    let(:example_rl) { RangeList.new [3, 5], [7, 8], [9, 13] }

    it '不重合' do
      expect(example_rl.query([14, 15])).to be_falsey
    end

    it '顺序不变' do
      expect(example_rl.query([1, 2])).to be_falsey
    end

    it '左接触' do
      expect(example_rl.query([13, 15])).to be_falsey
    end

    it '右接触' do
      expect(example_rl.query([1, 3])).to be_falsey
    end

    it '被包含' do
      expect(example_rl.query([10, 12])).to be_truthy
    end

    it '重合' do
      expect(example_rl.query([12, 15])).to be_falsey
    end

    it '刚好重合' do
      expect(example_rl.query([9, 13])).to be_truthy
    end

    it '包含' do
      expect(example_rl.query([2, 6])).to be_falsey
    end

    it '长跨越且包含' do
      expect(example_rl.query([2, 15])).to be_falsey
    end

    it '长跨越且重合' do
      expect(example_rl.query([6, 12])).to be_falsey
    end

    it '长跨越且接触' do
      expect(example_rl.query([7, 13])).to be_falsey
    end
  end

  describe 'print' do
    let(:example_rl) {  }

    it 'print empty' do
      example_rl = RangeList.new
      expect(example_rl.print).to eq('')
    end

    it 'print just one pair' do
      example_rl = RangeList.new [2, 5]
      expect(example_rl.print).to eq('[2, 5)')
    end

    it 'print more than one pair' do
      example_rl = RangeList.new [2, 5], [6, 8], [9, 12]
      expect(example_rl.print).to eq('[2, 5) [6, 8) [9, 12)')
    end
  end

  describe 'using the subject sample test cases' do
    let(:rl) { RangeList.new }

    it 'integration test' do
      rl.add [1, 5]
      expect(rl.print).to eq '[1, 5)'
      rl.add [10, 20]
      expect(rl.print).to eq '[1, 5) [10, 20)'
      rl.add [20, 20]
      expect(rl.print).to eq '[1, 5) [10, 20)'
      rl.add [20, 21]
      expect(rl.print).to eq '[1, 5) [10, 21)'
      rl.add [2, 4]
      expect(rl.print).to eq '[1, 5) [10, 21)'
      rl.add [3, 8]
      expect(rl.print).to eq '[1, 8) [10, 21)'
      rl.remove [10, 10]
      expect(rl.print).to eq '[1, 8) [10, 21)'
      rl.remove [10, 11]
      expect(rl.print).to eq '[1, 8) [11, 21)'
      rl.remove [15, 17]
      expect(rl.print).to eq '[1, 8) [11, 15) [17, 21)'
      rl.remove [3, 19]
      expect(rl.print).to eq '[1, 3) [19, 21)'
    end
  end
end
